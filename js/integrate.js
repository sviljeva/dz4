var pov = require('./integrator.js');

var prompt = require('prompt');

prompt.get (['f','a','b','n'],function (err,koef) {
	if (err) return console.log(err);
	console.log("f: ",koef.f);
	console.log("a: ",koef.a);
	console.log("b: ",koef.b);
	console.log("n: ",koef.n);

	koef.a = parseFloat(koef.a);
	koef.b = parseFloat(koef.b);
	koef.c = parseInt(koef.n);
	
	pov(koef.f,koef.a,koef.b,koef.n,function(err,rj){
		if (err)
			console.log(err);
		else 
			console.log("Povrsina je " + rj.Pov());
	});
});