module.exports = function (f,a,b,n,next) {

	var h = (b-a)/n;
	var s = 0;
	var sum = function () {
		for (var i = 1; i <= n; i++) {
			var ximj = a+h*(i-1);
			var xi = a+h*i;
			var x = (ximj + xi)/2;
			s += eval(f);
		}
		return s;
	};
	
	if (n<1) {
		next(new Error("n je broj ekvidistantnih točaka!"))
	}
	else next(null, {Pov : function () {return h*sum();}}
		);
};